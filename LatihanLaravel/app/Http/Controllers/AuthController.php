<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis(){
        return view('register');
    }

    public function signUp(Request $request){
        $first = $request['first'];
        $last = $request['last'];

        return view('welcome', ['first' => $first, 'last' => $last]);    }
}