@extends('master')

@section('judul')
<h1> SanberBook</h1>
@endsection

@section('content')
<h2>Social Media Developer Santai Berkulitas</h2>
<p>Belajar dan Berbagi agar hidup ini semakin santai dan berkulitas</p>
<h3>Benefit Join di SanberBook</h3>
<ul>
    <li>Mendapatkan motivasi dari sesama developer</li>
    <li>Sharing knowledge dari para mastah Sanber</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>
<h3>Cara Bergabung ke SanberBook</h3>
<ol>
    <li>Mengujungi website ini</li>
    <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
    <li>Selesai!</li>
</ol>
@endsection