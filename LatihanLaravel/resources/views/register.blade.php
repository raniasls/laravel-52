@extends('master')

@section('judul')
<h1>Buat Account Baru</h1>
@endsection

@section('content')
<h2>Sign Up Form</h2>
<form action="/welcome" method="post">
    @csrf
    <br>
    <label>First Name:</label><br>
    <input type="text" name = "first" ><br><br>
    <label>Last Name:</label><br>
    <input type="text" name = "last" ><br><br>
    <label>Gender:</label><br>
    <input type="radio" name="status"> Male<br>
    <input type="radio" name="status"> Female<br>
    <input type="radio" name="status"> Other<br><br>
    <label>Nationality:</label><br>
    <select name="Nationality">
        <option value="">Indonesian</option>
        <option value="">Singaporean</option>
        <option value="">Malaysian</option>
        <option value="">Australian</option>
    </select> <br><br>
    <label>Language Spoken:</label><br>
    <input type="checkbox" name="language">Indonesia<br>
    <input type="checkbox" name="language">English<br>
    <input type="checkbox" name="language">Other<br><br>
    <label>Bio:</label><br>
    <textarea cols="30" rows="10"></textarea> <br>

    <input type="submit" value="Sign Up">
</form>
@endsection