@extends('master')

@section('judul')
Biodata Cast
@endsection

@section('content')

<h1>{{$cast ->name}} </h1>
<p>{{$cast ->bio}} <p>

<a href="/cast" class='btn btn-primary btn-sm'>Kembali</a>

@endsection