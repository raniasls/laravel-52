@extends('master')

@section('judul')
List Cast
@endsection

@section('content')

<a href="/cast/create" class='btn btn-dark btn-sm mb-3'>Tambah</a>

<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value)
        <tr>
            <td> {{$key + 1}} </td>
            <td> {{$value ->name}} </td>
            <td> {{$value ->umur}} </td>
            <td> 
              <form action="/cast/{{$value ->id}}" method="POST">
                @csrf
                @method('delete')
                <a href="/cast/{{$value ->id}}" class='btn btn-secondary btn-sm'>Biodata</a>
                <a href="/cast/{{$value ->id}}/edit" class='btn btn-light btn-outline-secondary btn-sm'>Edit</a>
                <input type="submit" value="Delete" class = "btn btn-dark btn-sm">
              </form>
            </td>
        </tr>
    @empty
        <tr>
            <td>Tidak ada data</td>
        </tr>
    @endforelse
    </tbody>
  </table>

@endsection