<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, ('home')]);

Route::get('/register', [AuthController::class, ('regis')]);

Route::post('/welcome', [AuthController::class, ('signUp')]);

Route::get('/dataTable', function(){
    return view('dataTable');
});

Route::get('/table', function(){
    return view('table');
});

//CRUD Cast

//Create
//Add Cast
Route::get('/cast/create', [CastController::class, ('create')]);
//Submit
Route::post('/cast', [CastController::class, ('store')]);

//Read
//All Data
Route::get('/cast', [CastController::class, ('index')]);
//Biodata
Route::get('/cast/{cast_id}', [CastController::class, ('show')]);

//Update
//Form Update
Route::get('/cast/{cast_id}/edit', [CastController::class, ('edit')]);
//Update data ke database
Route::put('/cast/{cast_id}', [CastController::class, ('update')]);

//Delete
//Delete Based on id
Route::delete('/cast/{cast_id}', [CastController::class, ('destroy')]);