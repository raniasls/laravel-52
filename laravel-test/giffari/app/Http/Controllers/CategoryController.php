<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function show(){
        return view('category.create');
    }

    public function create(Request $request){
        $request->validate([
            'name' => 'required'
        ]);
    }
}
